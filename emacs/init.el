;;
;; init.el for emacs
;;
;; Made by thomas
;; Login   thomas <thomas.tych@gmail.com>
;;
;; Started on  Wed Feb 15 21:59:19 2017 thomas
;; Last update Thu Feb 16 14:05:02 2017 thomas
;;


;;==============================================================================
;;  PACKAGES
;;==============================================================================
(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list 'package-archives
               '("melpa" . "https://stable.melpa.org/packages/") t)
  (package-initialize)
  (global-set-key (kbd "C-x P") 'list-packages)
  )
  ; [Enter ↵] (package-menu-describe-package) → Describe the package under cursor.
  ; [i] (package-menu-mark-install) → mark for installation.
  ; [u] (package-menu-mark-unmark) → unmark.
  ; [d] (package-menu-mark-delete) → mark for deletion (removal of a installed package).
  ; [x] (package-menu-execute) → for “execute” (start install/uninstall of marked items).
  ; [r] (package-menu-refresh) → refresh the list from server.
  ; (For complete list of keys, call describe-mode [Ctrl+h m])


;;==============================================================================
;;  LOAD PATH
;;==============================================================================
;; load-path
(add-to-list 'load-path (expand-file-name "~/.emacs.d/custom"))
(add-to-list 'load-path (expand-file-name "~/.emacs.d/vendor"))


;;==============================================================================
;;  ID
;;==============================================================================
(setq user-login (getenv "USER")
	  user-full-name "thomas"
      user-mail-address (getenv "USER_EMAIL"))
      ;user-mail-address "user@address")


;;==============================================================================
;;  ENVIRONMENT
;;==============================================================================
(defconst *is-unix* (member system-type '(freebsd)))
(defconst *is-a-mac* (eq system-type 'darwin))
(defconst *is-linux* (member system-type '(gnu gnu/linux gnu/kfreebsd)))


;;==============================================================================
;;  KEYBINDINGS
;;==============================================================================
;; Assume caps is mapped to control
;; (setq
;;  ns-command-modifier 'meta              ; L command -> C
;;  ns-option-modifier 'meta               ; L option -> m
;;  ;; ns-control-modifier 'super          ; L control -> super
;;  ;; ns-function-modifier 'hyper         ; fn -> super
;;  ns-function-modifier 'super

;;  ;; right hand side modifiers
;;  ns-right-command-modifier 'super       ; R command -> super
;;  ns-right-option-modifier 'hyper        ; R option -> hyper
;;  )


;;==============================================================================
;;  CUSTOM BASE
;;==============================================================================
;;; Allow more resource usage
;; increase max-specpdl-size
;; increase max-lisp-eval-depth

;(set-background-color "blue")

;; Show time on the mode line
;(display-time-mode 1)
;(setq display-time-format "%H:%M:%S")
;;------------------------------------------------------------------------------
;;;; Cursor
;; Cursor shapes are defined in
;; ‘/usr/include/X11/cursorfont.h’;
;; for example, the ‘target’ cursor is number 128;
;; the ‘top_left_arrow’ cursor is number 132.
(let ((mpointer (x-get-resource "*mpointer"
                                "*emacs*mpointer")))
;; If you have not set your mouse pointer
;; then set it, otherwise leave as is:
  (if (eq mpointer nil)
      (setq mpointer "132")) ; top_left_arrow
  (setq x-pointer-shape (string-to-int mpointer))
  (set-mouse-color "white"))
;; No cursor blink
(if (fboundp 'blink-cursor-mode)
    (blink-cursor-mode -1))
;
;; Kill UI cruft
;(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
;(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
;(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
;; want menu bar only in guis, it's useless on the terminal anyways
;(cond ((not window-system)
;       (menu-bar-mode -1)))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auto-compression-mode t)
 '(auto-image-file-mode t)
 '(auto-save-default t)
 '(backward-delete-char-untabify-method nil)
 '(blink-cursor-mode nil)
 '(change-major-mode-with-file-name t)
 '(column-number-mode t)
 '(delete-selection-mode t)
 '(delete-trailing-lines t)
 '(delete-trailing-whitespace t)
 '(eval-expression-debug-on-error t)
 '(fill-column 80)
 '(font-lock-auto-fontify t)
 '(font-lock-maximum-decoration t)
 '(font-lock-maximum-size nil)
 '(font-lock-mode t t)
 '(font-lock-use-colors (quote (color)))
 '(font-lock-use-fonts (quote (or (mono) (grayscale))))
 '(global-hl-line-mode t)
 '(indent-tabs-mode t)
 '(indicate-empty-lines t)
 '(inhibit-startup-buffer-menu t)
 '(inhibit-startup-echo-area-message "")
 '(inhibit-startup-screen t)
 '(initial-major-mode (quote fundamental-mode))
 '(initial-scratch-message nil)
 '(kill-ring-max 100)
 '(latin1-display t nil (latin1-disp))
 '(line-number-mode t)
 '(major-mode (quote fundamental-mode))
 '(make-backup-files nil)
 '(menu-bar-mode nil)
 '(mouse-wheel-mode t)
 '(next-line-add-newlines nil)
 '(package-selected-packages (quote (ruby-end rubocop minitest)))
 '(ring-bell-function (quote ignore))
 '(scroll-bar-mode nil)
 '(scroll-preserve-screen-position t)
 '(select-enable-clipboard t)
 '(show-trailing-whitespace t)
 '(speedbar-show-unknown-files t)
 '(tab-width 4)
 '(tabbar-use-images t)
 '(tool-bar-mode nil)
 '(tooltip-delay 0.1)
 '(tooltip-mode nil)
 '(transient-mark-mode t)
 '(vc-follow-symlinks t)
 '(x-select-enable-clipboard-manager t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Redefine startup messasge, replace the string with whatever you want
;(defun startup-echo-area-message ()
;  "Emacs ready.")

;; y/n prompts instead of yes/no
;(fset 'yes-or-no-p 'y-or-n-p)
(defalias 'yes-or-no-p 'y-or-n-p)


;;==============================================================================
;;  Dialog Box
;;==============================================================================
;; No popups and dialogues.
;; Not to mention that they're incredibly annoying.
(defadvice y-or-n-p (around prevent-dialog activate)
  "Prevent y-or-n-p from activating a dialog"
  (let ((use-dialog-box nil))
    ad-do-it))
(defadvice y-or-n-p (around prevent-dialog-yorn activate)
  "Prevent y-or-n-p from activating a dialog"
  (let ((use-dialog-box nil))
    ad-do-it))
;; Fallback. DIE, DIALOGUE BOXES, DIE!!
(setq use-dialog-box nil)


;;==============================================================================
;;  Global Mode
;;==============================================================================
;; detect file changes and update buffer if not changes in buffer
(global-auto-revert-mode t)
;; incremental MiniBuffer Completion
(icomplete-mode 1)
;; interactively do things with buffers and file
;(require 'ido) ; (autoload 'ido "ido")
;(ido-mode t)
;(ido-everywhere 1)
;(setq ido-enable-flex-matching t
;      ido-use-virtual-buffers t
;      ido-use-faces nil)
;; linum mode : displaying line numbers in a buffer
(global-linum-mode 1)
;(setq linum-format "%d ")
;(setq linum-format "%4d \u2502 ")

(setq grep-command "grep -i -nH -e ")

(setq find-file-existing-other-name t)

(set-language-environment "latin-1")
;; Remember you can enable or disable multilingual text input
;; with the toggle-input-method’ (C-\) command
(setq default-input-method "latin-1-prefix")

(load "tabbar")
(tabbar-mode t)

;;==============================================================================
;;  Custom
;;==============================================================================
;; ext
(load "f_assoc.custom.el")

;; Macro
(load "macro.custom.el")

;; Shortcut
(load "shortcut.custom.el")

;; org mode custom
(load "org.custom.el")

;; development
(load "development.custom.el")

;; term
(load "term.custom.el")
