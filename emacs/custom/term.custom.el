;; term mode
(add-hook
 'term-mode-hook
 (lambda()
   (setq-local show-trailing-whitespace nil)
   (setq-local global-hl-line-mode nil)
   ))

;; eshell mode
(add-hook
 'eshell-mode-hook
 (lambda()
   (setq-local show-trailing-whitespace nil)
   (setq-local global-hl-line-mode nil)
   ))
