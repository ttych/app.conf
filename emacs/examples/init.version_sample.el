;;; Load custom file from another path
;(setq custom-file "~/.emacs-custom.el")

;;; Load custom file depending on version
(cond ((< emacs-major-version 22)
      	  ;; Emacs 21 customization.
     	  (setq custom-file "~/.custom-21.el"))
      ((and (= emacs-major-version 22)
      	       (< emacs-minor-version 3))
	    ;; Emacs 22 customization, before version 22.3.
            (setq custom-file "~/.custom-22.el"))
      (t
            ;; Emacs version 22.3 or later.
            (setq custom-file "~/.emacs-custom.el")))

(load custom-file)
