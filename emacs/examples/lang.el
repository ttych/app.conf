
; C
(add-hook 'c-mode-hook (lambda () (add-to-list 'write-file-functions 'delete-trailing-whitespace)))
(add-hook 'c-mode-common-hook (lambda () (add-to-list 'write-file-functions 'delete-trailing-whitespace)))
(add-hook 'c-mode-hook (function (lambda () (setq c-basic-offset 4))))
(add-hook 'c-mode-common-hook (function (lambda () (setq tab-width 4))))

; Shell
(add-hook 'shell-script-hook (lambda () (add-to-list 'write-file-functions 'delete-trailing-whitespace)))
(add-hook 'shell-script-mode-hook (lambda () (add-to-list 'write-file-functions 'delete-trailing-whitespace)))
(add-hook 'sh-mode-hook (lambda () (add-to-list 'write-file-functions 'delete-trailing-whitespace)))

; Lisp
;(add-hook 'lisp-mode-hook (lambda () (add-to-list 'write-file-functions 'delete-trailing-whitespace)))
;(add-hook 'emacs-lisp-mode-hook (lambda () (add-to-list 'write-file-functions 'delete-trailing-whitespace)))

; Perl
(add-hook 'perl-mode-hook (lambda () (add-to-list 'write-file-functions 'delete-trailing-whitespace)))
(add-hook 'cperl-mode-hook (lambda () (add-to-list 'write-file-functions 'delete-trailing-whitespace)))

; Ruby
(add-hook 'ruby-mode-hook (lambda () (add-to-list 'write-file-functions 'delete-trailing-whitespace)))
(add-hook 'ruby-mode-hook #'rubocop-mode)

; Makefile
(add-hook 'makefile-mode-hook (lambda () (add-to-list 'write-file-functions 'delete-trailing-whitespace)))

; yaml
(require 'yaml-mode)
(add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))
