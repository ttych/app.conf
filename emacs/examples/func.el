;;
;; func.el for emacs
;;
;; Made by Thomas TYCHENSKY
;; Login   ttichens <>
;;
;; Started on  Fri Mar 21 14:34:33 2014 ttichens
;; Last update Mon Apr 25 16:50:06 2016 ttichens
;;

; insert time function
(defun do_insert_time ()
  (interactive)
 (insert-string (current-time-string)))
(set-variable 'c-argdecl-indent   0)

(setq header-made-by    "Made by "
      header-login      "Login   "
      header-login-beg  "<"
      header-login-mid  "@"
      header-login-end  ">"
      header-started    "Started on  "
      header-last       "Last update "
      header-for        " for "
      header-in         " in ")

(setq user-login (getenv "USER"))
(setq user-fname (user-full-name))
(setq user-email (getenv "USER_EMAIL"))

(setq write-file-hooks (cons 'update-std-header write-file-hooks))

(setq std-c-alist               '( (cs . "/*") (cc . "** ") (ce . "*/") )
      std-css-alist             '( (cs . "/*") (cc . "** ") (ce . "*/") )
      std-cpp-alist             '( (cs . "//") (cc . "// ") (ce . "//") )
      std-pov-alist             '( (cs . "//") (cc . "// ") (ce . "//") )
      std-java-alist            '( (cs . "//") (cc . "// ") (ce . "//") )
      std-latex-alist           '( (cs . "%%") (cc . "%% ") (ce . "%%") )
      std-lisp-alist            '( (cs . ";;") (cc . ";; ") (ce . ";;") )
      std-xdefault-alist        '( (cs . "!!") (cc . "!! ") (ce . "!!") )
      std-pascal-alist          '( (cs . "{ ") (cc . "   ") (ce . "}" ) )
      std-makefile-alist        '( (cs . "##") (cc . "## ") (ce . "##") )
      std-text-alist            '( (cs . "##") (cc . "## ") (ce . "##") )
      std-fundamental-alist     '( (cs . "  ") (cc . "   ") (ce . "  ") )
      std-html-alist            '( (cs . "<!--") (cc . "  -- ") (ce . "-->"))
      std-nroff-alist           '( (cs . "\\\"") (cc . "\\\" ") (ce . "\\\""))
      std-sscript-alist         '( (cs . "#!/bin/sh")  (cc . "## ") (ce . "##") )
      std-perl-alist            '( (cs . "#!/usr/bin/perl -w")  (cc . "## ")(ce . "##") )
      std-cperl-alist           '( (cs . "#!/usr/bin/perl -w")  (cc . "## ")(ce . "##") )
      std-php-alist             '( (cs . "<?php") (cc . "// ")(ce . "?>") )
      std-org-alist            '( (cs . "##") (cc . "## ") (ce . "##") )
      std-ruby-alist            '( (cs . "#!/usr/bin/env ruby")  (cc . "## ")(ce . "##") )
      )

(setq std-modes-alist '(("C/l"                  . std-c-alist)
                        ("CSS"                  . std-c-alist)
                        ("PoV"                  . std-pov-alist)
                        ("C++"                  . std-cpp-alist)
                        ("Lisp"                 . std-lisp-alist)
                        ("Lisp Interaction"     . std-lisp-alist)
                        ("Emacs-Lisp"           . std-lisp-alist)
                        ("Fundamental"          . std-fundamental-alist)
                        ("Shell-script"         . std-sscript-alist)
                        ("Makefile"             . std-makefile-alist)
                        ("Perl"                 . std-cperl-alist)
                        ("CPerl"                . std-cperl-alist)
                        ("xdefault"             . std-xdefault-alist)
                        ("java"                 . std-java-alist)
                        ("latex"                . std-latex-alist)
                        ("Pascal"               . stdp-ascal-alist)
                        ("Text"                 . std-text-alist)
                        ("HTML"                 . std-html-alist)
                        ("Nroff"                . std-nroff-alist)
                        ("TeX"                  . std-latex-alist)
                        ("PHP"                  . std-php-alist)
                        ("LaTeX"                . std-latex-alist)
                        ("Org"                  . std-org-alist)
			("Ruby"                 . std-ruby-alist)
		       )
      )

(defun std-get (a)
  (interactive)
  (cdr (assoc a (eval (cdr (assoc mode-name std-modes-alist)))))
  )

(defun update-std-header ()
  "Updates std header with last modification time & owner.\n(According to mode)"
  (interactive)
  (save-excursion
    (if (buffer-modified-p)
        (progn
          (goto-char (point-min))
          (if (search-forward header-last nil t)
              (progn
;               (delete-region (point-at-bol) (point-at-eol))
                (delete-region
                 (progn (beginning-of-line) (point))
                 (progn (end-of-line) (point)))
                (insert-string (concat (std-get 'cc)
                                       header-last
                                       (current-time-string)
                                       " "
                                       user-login))
                (message "Last modification header field updated."))))))
  nil)

(defun std-file-header ()
  "Puts a standard header at the beginning of the file.\n(According to mode)"
  (interactive)
  (goto-char (point-min))
  (let ((projname "toto")(location "titi"))
    (setq projname (read-from-minibuffer
                    (format "Type project name (RETURN to quit) : ")))
    (setq location (getenv "PWD"))

    (insert-string (std-get 'cs))
    (newline)
    (insert-string (concat (std-get 'cc)
                           (buffer-name)
                           header-for
                           projname))
;;                         header-in
;;                         location))
    (newline)
    (insert-string (std-get 'cc))
    (newline)
    (insert-string (concat (std-get 'cc) header-made-by user-fname))
    (newline)
    (insert-string (concat (std-get 'cc)
                           header-login
			   user-login
			   " "
                           header-login-beg
			   user-email
;                           user-login
;                           header-login-mid
;                           domaine-name
                           header-login-end))
    (newline)
    (insert-string (std-get 'cc))
    (newline)
    (insert-string (concat (std-get 'cc)
                           header-started
                           (current-time-string)
                           " "
                           user-login))
    (newline)
    (insert-string (concat (std-get 'cc)
                           header-last
                           (current-time-string)
                           " "
                           user-login))
    (newline)
    (insert-string (std-get 'ce))
    (newline)))

(defun insert-std-vertical-comments ()
  "Inserts vertical comments (according to mode)."
  (interactive)
  (beginning-of-line)
  (insert-string (std-get 'cs))
  (newline)
  (let ((ok t)(comment ""))
    (while ok
      (setq comment (read-from-minibuffer
                     (format "Type comment (RETURN to quit) : ")))
      (if (= 0 (length comment))
          (setq ok nil)
        (progn
          (insert-string (concat (std-get 'cc) comment))
          (newline)))))
  (insert-string (std-get 'ce))
  (newline))

(defun std-toggle-comment ()
  "Toggles line comment on or off (according to mode)."
  (interactive)
  (save-excursion
    (let (beg end)
      (beginning-of-line)
      (setq beg (point))
      (end-of-line)
      (setq end (point))
      (save-restriction
        (if (not (equal beg end))
            (progn
              (narrow-to-region beg end)
              (goto-char beg)
              (if (search-forward (std-get 'cs) end t)
                  (progn
                    (beginning-of-line)
                    (replace-string (std-get 'cs) "")
                    (replace-string (std-get 'ce) ""))
                (progn
                  (beginning-of-line)
                  (insert-string (std-get 'cs))
                  (end-of-line)
                  (insert-string (std-get 'ce)))))))))
  ;;  (indent-according-to-mode)
  (indent-for-tab-command)
  (next-line 1))

