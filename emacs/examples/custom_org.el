
; TODO sequence with \C-c \C-t
;(setq org-todo-keywords
;      '((sequence "TODO" "FEEDBACK" "VERIFY" "|" "DONE" "DELEGATED")))
;(setq org-todo-keywords
;      '((sequence "TODO(t)" "PENDING(w@/!)" "VERIFY" "|" "DONE(d@/!)" "DELEGATED(D@/!)")
;	(sequence "REPORT(r)" "BUG(b)" "KNOWNCAUSE(k)" "|" "FIXED(f)")
;	(sequence "|" "CANCELED(c@)")))
(setq org-todo-keywords
      '((sequence "TODO" "IN-PROGRESS" "WAITING" "TEST" "|" "DELEGATED" "DONE")))

; log - time tracking
(setq org-log-done 'time)
;(setq org-log-done 'note)
; adding special markers ‘!’ (for a timestamp) and ‘@’ (for a note) in parentheses after each keyword
