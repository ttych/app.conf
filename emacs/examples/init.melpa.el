(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list
   'package-archives
   ;; '("melpa" . "http://melpa.org/packages/")
   '("melpa-stable" . "https://stable.melpa.org/packages/")
   t)
  (package-initialize))
