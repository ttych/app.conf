
; compare 2 windows
(global-set-key "\C-cc" 'compare-windows)

; search regexp
(global-set-key "\C-co" 'occur)

; speedbar
(global-set-key "\C-cs" 'speedbar)

; goto
(global-set-key "\C-cg" 'goto-line)

; compile
(global-set-key "\C-c\C-c" 'compile)

; prevent accidental C-x-f
(global-unset-key "\C-xf")

; add local-variable for emacs
(global-set-key "\C-ce" 'add-file-local-variable)

; header
(global-set-key "\C-ch" 'std-file-header)
;(global-set-key "\C-c\C-h" 'update-std-header)
(global-set-key "\C-ct" 'do_insert_time)
